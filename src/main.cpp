#include <iostream>

using namespace std;

int main()
{
     // decaration de variable
    int vitesse;
    int distance;
    float temps;
    int heure;
    int minute;
    float temps2;

    cout << "Calcul du temps de parcours d'un trajet : " << endl;
    cout << "vitesse moyenne(en km/h) : \t " ;
    cin >> vitesse ;
    cout << "Distance a parcourir (en km) : \t " ;
    cin >> distance ;

    //calcul
    temps = distance * 3600 / vitesse ;
    heure = temps / 3600;
    cout << heure << "H" << endl;
    temps2 = heure * 3600;
    cout << temps2<< "s" << endl;
    temps = temps - temps2;
    cout << temps << "s" << endl;
    minute = temps / 60;
    cout << minute << "min" << endl;
    temps2 = minute * 60;
    cout << temps2 << "s" << endl;
    temps = temps - temps2;
    cout << temps << "s" << endl;

    cout << "A" << vitesse << "km/h, une distance de" << distance
    << "km est parcourue en" << heure << "H " << minute << "min " << temps << "s" ;


    return 0;
}
